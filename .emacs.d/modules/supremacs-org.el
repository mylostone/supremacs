;;; supremacs-org.el --- Emacs Supremacs: org-mode configuration.
;;
;; Copyright © 2011-2022 Bozhidar Batsov
;;
;; Author: Bozhidar Batsov <bozhidar@batsov.com>
;; URL: https://github.com/bbatsov/supremacs

;; This file is not part of GNU Emacs.

;;; Commentary:

;; Some basic configuration for org-mode.

;;; License:

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Code:

(require 'org)
(require 'org-habit)

(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))

;; a few useful global keybindings for org-mode
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-switchb)

(setq org-log-done t)
(setq org-log-into-drawer t)

(defun supremacs-org-mode-defaults ()
  (let ((oldmap (cdr (assoc 'supremacs-mode minor-mode-map-alist)))
        (newmap (make-sparse-keymap)))
    (set-keymap-parent newmap oldmap)
    (define-key newmap (kbd "C-c +") nil)
    (define-key newmap (kbd "C-c -") nil)
    (define-key newmap (kbd "C-a") 'org-beginning-of-line)
    (define-key newmap (kbd "C-M-<return>") 'org-insert-heading-respect-content)
    (make-local-variable 'minor-mode-overriding-map-alist)
    (push `(supremacs-mode . ,newmap) minor-mode-overriding-map-alist))
)
;;(define-key org-mode-map (kbd "C-M-<return>") 'org-insert-heading-respect-content)
;; ;; #+begin_src elisp
;; ;;
;; ;; (with-eval-after-load "org"
;; ;;
;; ;;   (define-key org-mode-map (kbd "C-M-<return>")
;; ;;     'org-insert-heading-respect-content))
;; ;; (with-eval-after-load "org"
;; ;;   (define-key org-mode-map (kbd "C-c C-j") #'counsel-org-goto))
;; ;;
;; ;; #+end_src elisp
;; ;;
(setq supremacs-org-mode-hook 'supremacs-org-mode-defaults)

(add-hook 'org-mode-hook (lambda () (run-hooks 'supremacs-org-mode-hook)))

;; FIXME: make this a customizable variable. perhaps make a toggle function.
(setq supremacs-org-mode-export-on-save-pdf-tex-default t)
(defun supremacs-org-mode-hook()
  (make-variable-buffer-local 'supremacs-org-mode-export-on-save-pdf-tex)
  (setq supremacs-org-mode-export-on-save-pdf-tex supremacs-org-mode-export-on-save-pdf-tex-default)
  )
(add-hook 'org-mode-hook 'supremacs-org-mode-hook)

(defun supremacs-org-mode-after-save-hook ()
  ;;  (async-start
  ;;   (lambda ()
  (message "%s : %S" major-mode (current-buffer))
  (if (string-match "org-mode" (format "%s" major-mode))
      (progn
        (message "supremacs org-mode buffer beginning exporting to pdf on after-save-hook"
                 (org-latex-export-to-pdf))
        (message "supremacs org-mode buffer completed exporting to pdf on after-save-hook")
        )
    ;;(message "not an org-mode buffer")

    (if (string-match "foo-bar" (format "%s" major-mode))
        ;; (org-latex-export-to-pdf)
        (message "a foo-bar buffer") ))
  )
;;   )
;;  )


;;(defun supremacs-initialize-hooks()
  (add-hook 'after-save-hook 'supremacs-org-mode-after-save-hook)
;;  )



(provide 'supremacs-org)

;;; supremacs-org.el ends here
