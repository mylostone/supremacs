# Support

Supremacs currently has several official & unofficial support channels.

For questions, suggestions and support refer to one of them.  Please, don't
use the support channels to report issues, as this makes them harder to track.

## Gitter

Most internal discussions about the development of Supremacs happen on its
[gitter channel](https://gitter.im/bbatsov/supremacs).  You can often find
Supremacs's maintainers there and get some interesting news from the project's
kitchen.

## Mailing List

The [official mailing list](https://groups.google.com/forum/#!forum/emacs-supremacs) is
hosted at Google Groups. It's a low-traffic list, so don't be too hesitant to subscribe.

## Freenode

If you're into IRC you can visit the `#supremacs` channel on Freenode.
It's not actively
monitored by the Supremacs maintainers themselves, but still you can get support
from other Supremacs users there.

## Stackoverflow

We're also encouraging users to ask Supremacs-related questions on StackOverflow.

When doing so you should use the
[emacs-supremacs](https://stackoverflow.com/questions/tagged/emacs-supremacs) tag.
