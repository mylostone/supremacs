# this is to bootstrap the user's default emacs settings to point to the supremacs
# settings at ~/dev/gitlab.com/mylostone/supremacs/.emacs.d/

realpath=$(realpath -s --relative-to=$HOME $0)
echo "realpath=$realpath" 1>&2
dirname=$(dirname $realpath)
echo "dirname=$dirname" 1>&2
echo ln -s $dirname $HOME/.emacs.d
