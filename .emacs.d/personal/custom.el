(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(blink-cursor-mode t)
 '(comint-input-ignoredups t)
 '(dired-listing-switches "-lahsi")
 '(fill-column 80)
 '(focus-follows-mouse t)
 '(global-display-line-numbers-mode t)
 '(global-hl-line-mode t)
 '(global-hl-line-sticky-flag t)
;; '(magit-git-global-arguments
;;   '("--no-pager" "--literal-pathspecs" "-c" "core.preloadindex=true" "-c" "log.showSignature=false" "-c" "color.ui=false" "-c" "color.diff=false"))
 '(org-list-allow-alphabetical t)
 '(package-archives
   '(("gnu" . "https://elpa.gnu.org/packages/")
     ("melpa" . "https://melpa.org/packages/")))
 '(package-selected-packages
   '(geiser company-anaconda anaconda-mode csv-mode kotlin-mode async sr-speedbar magit lsp-java pdf-tools tablist copyit copyit-pandoc flycheck flymake flyspell simple-httpd impatient-mode))
 '(show-paren-mode t)
 '(show-paren-style 'expression)
 '(sr-speedbar-default-width 64)
 '(sr-speedbar-max-width 64)
 '(sr-speedbar-right-side nil)
 '(warning-suppress-log-types '((mule) (mule)))
 '(warning-suppress-types '((mule))))





(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DejaVu Sans Mono" :foundry "PfEd" :slant normal :weight normal :height 180 :width normal)))))
;; FIXME: make this a more reasonable (require) or some such
;;(load-file "/home/user/dev/gitlab.com/mylostone/contrib.git/emacs/supremacs/supremacs-init.el")


(defun supremacs-initialize-faces()
  (custom-set-faces
   ;; custom-set-faces was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.

   ;;;;;;;;;;;;;;;;;;;;
   ;; qubes/debian emacs-lucid
   ;;
   ;; FIXME: Calculate :height based on current screen resolution and desired actually displayed font size
   '(default ((t (:family "DejaVu Sans Mono" :foundry "PfEd" :slant normal :weight normal :height 180 :width normal))))
   ;;
   ;;;;;;;;;;;;;;;;;;;;

   ;;;;;;;;;;;;;;;;;;;;
   ;; <unknown> emacs-lucid
   ;;
   ;; '(default ((t (:family "fixed" :foundry "misc" :slant normal :weight normal :height 181 :width normal))))
   ;;
   ;;;;;;;;;;;;;;;;;;;;


   ;;;;;;;;;;;;;;;;;;;;
   ;; artix emacs-lucid
   ;; still a little bit small but at least is readable, also the same when using this on qubes/debian
   ;; '(default ((t (:family "fixed" :foundry "misc" :slant normal :weight normal :height 240 :width normal))))
   ;;
   ;;;;;;;;;;;;;;;;;;;;

   )
  ;; set-frame-font: Font not available: #<font-spec nil misc fixed ## iso8859-1 medium r normal 24 nil 110 120 ((:name . "-misc-fixed-medium-r-normal--24-*-*-*-c-120-iso8859-1") (:user-spec . "-misc-fixed-medium-r-normal--24-*-*-*-c-120-iso8859-1"))>

  )
