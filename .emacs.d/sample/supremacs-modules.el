;;; supremacs-modules.el --- A listing of modules to load on startup
;;
;; Copyright © 2011-2022 Bozhidar Batsov
;;
;; Author: Bozhidar Batsov <bozhidar@batsov.com>
;; URL: https://github.com/bbatsov/supremacs

;; This file is not part of GNU Emacs.

;;; Commentary:

;; This file is just a list of Supremacs modules to load on startup.
;; For convenience the modules are grouped in several categories.
;; The supremacs-modules.el in the samples folder should be copied
;; to your personal folder and edited there.

;; Note that some modules can't be used together - e.g. you shouldn't
;; enable both supremacs-ido and supremacs-ivy, as they serve the same
;; purpose.

;;; License:

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Code:

;;; Uncomment the modules you'd like to use and restart Supremacs afterwards

;;; General productivity tools

;; (require 'supremacs-ido) ;; Supercharges Emacs completion for C-x C-f and more
(require 'supremacs-ivy) ;; A mighty modern alternative to ido
;; (require 'supremacs-selectrum) ;; A powerful, yet simple, alternative to ivy
;; (require 'supremacs-helm) ;; Interface for narrowing and search
;; (require 'supremacs-helm-everywhere) ;; Enable Helm everywhere
(require 'supremacs-company)
;; (require 'supremacs-key-chord) ;; Binds useful features to key combinations

;;; Vim emulation
;;
;; Enable this module if you're fond of vim's keybindings.
;; (require 'supremacs-evil)

;;; Org-mode (a legendary productivity tool that deserves its own category)
;;
;; Org-mode helps you keep TODO lists, notes and more.
(require 'supremacs-org)

;;; Programming languages support
;;
;; Modules for a few very common programming languages
;; are enabled by default.

(require 'supremacs-c)
;; (require 'supremacs-clojure)
;; (require 'supremacs-coffee)
;; (require 'supremacs-common-lisp)
(require 'supremacs-css)
;; (require 'supremacs-dart)
(require 'supremacs-emacs-lisp)
;; (require 'supremacs-erlang)
;; (require 'supremacs-elixir)
;; (require 'supremacs-fsharp)
;; (require 'supremacs-go)
;; (require 'supremacs-haskell)
(require 'supremacs-js)
;; (require 'supremacs-latex)
(require 'supremacs-lisp) ;; Common setup for Lisp-like languages
;; (require 'supremacs-literate-programming) ;; Setup for Literate Programming
(require 'supremacs-lsp) ;; Base setup for the Language Server Protocol
;; (require 'supremacs-lua)
;; (require 'supremacs-ocaml)
(require 'supremacs-perl)
;; (require 'supremacs-python)
;; (require 'supremacs-racket)
;; (require 'supremacs-ruby)
;; (require 'supremacs-rust)
;; (require 'supremacs-scala)
;; (require 'supremacs-scheme)
(require 'supremacs-shell)
;; (require 'supremacs-scss)
;; (require 'supremacs-ts)
(require 'supremacs-web) ;; Emacs mode for web templates
(require 'supremacs-xml)
(require 'supremacs-yaml)

;;; Misc
(require 'supremacs-erc) ;; A popular Emacs IRC client (useful if you're still into Freenode)

(provide 'supremacs-modules)
;;; supremacs-modules.el ends here
