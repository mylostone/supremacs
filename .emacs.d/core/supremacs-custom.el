;;; supremacs-custom.el --- Emacs Supremacs: Supremacs's customizable variables.
;;
;; Copyright © 2011-2022 Bozhidar Batsov
;;
;; Author: Bozhidar Batsov <bozhidar@batsov.com>
;; URL: https://github.com/bbatsov/supremacs

;; This file is not part of GNU Emacs.

;;; Commentary:

;; Refinements of the core editing experience in Emacs.

;;; License:

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Code:

;; customize
(defgroup supremacs nil
  "Emacs Supremacs configuration."
  :prefix "supremacs-"
  :group 'convenience)

(defcustom supremacs-minimalistic-ui nil
  "Controls whether to display the menu-bar and line numbers.
Note that the toolbar is always hidden regardless of this setting."
  :type 'boolean
  :group 'supremacs
  :package-version '(supremacs . "1.1"))

(defcustom supremacs-super-keybindings t
  "Controls whether to use the Super key in keybindings.
They can be problematic in some operating systems (e.g. Windows)
or desktop environments that make heavy use of them."
  :type 'boolean
  :group 'supremacs
  :package-version '(supremacs . "1.1"))

(defcustom supremacs-auto-save t
  "Non-nil values enable Supremacs's auto save."
  :type 'boolean
  :group 'supremacs)

(defcustom supremacs-guru t
  "Non-nil values enable `guru-mode'."
  :type 'boolean
  :group 'supremacs)

(defcustom supremacs-whitespace t
  "Non-nil values enable Supremacs's whitespace visualization."
  :type 'boolean
  :group 'supremacs)

(defcustom supremacs-clean-whitespace-on-save t
  "Cleanup whitespace from file before it's saved.
Will only occur if `supremacs-whitespace' is also enabled."
  :type 'boolean
  :group 'supremacs)

(defcustom supremacs-flyspell t
  "Non-nil values enable Supremacs's flyspell support."
  :type 'boolean
  :group 'supremacs)

(defcustom supremacs-user-init-file (expand-file-name "personal/"
                                                    user-emacs-directory)
  "Path to your personal customization file.
Supremacs recommends you only put personal customizations in the
personal folder.  This variable allows you to specify a specific
folder as the one that should be visited when running
`crux-find-user-init-file'.  This can be easily set to the desired buffer
in Lisp by putting `(setq supremacs-user-init-file load-file-name)'
in the desired elisp file."
  :type 'string
  :group 'supremacs)

(defcustom supremacs-indent-sensitive-modes
  '(conf-mode coffee-mode haml-mode python-mode slim-mode yaml-mode)
  "Modes for which auto-indenting is suppressed."
  :type 'list
  :group 'supremacs)

(defcustom supremacs-format-on-save t
  "Run mode specific format on file before it's saved.
Currently only applies to tide-mode."
  :type 'boolean
  :group 'supremacs)

(defcustom supremacs-yank-indent-modes '(LaTeX-mode TeX-mode)
  "Modes in which to indent regions that are yanked (or yank-popped).
Only modes that don't derive from `prog-mode' should be listed here."
  :type 'list
  :group 'supremacs)

(defcustom supremacs-yank-indent-threshold 1000
  "Threshold (# chars) over which indentation does not automatically occur."
  :type 'number
  :group 'supremacs)

(defcustom supremacs-theme 'zenburn
  "The default color theme, change this in your /personal/preload config."
  :type 'symbol
  :group 'supremacs)

(provide 'supremacs-custom)

;;; supremacs-custom.el ends here
