;;; supremacs-core.el --- Emacs Supremacs: Core Supremacs functions.
;;
;; Copyright © 2011-2022 Bozhidar Batsov
;;
;; Author: Bozhidar Batsov <bozhidar@batsov.com>
;; URL: https://github.com/bbatsov/supremacs

;; This file is not part of GNU Emacs.

;;; Commentary:

;; Here are the definitions of most of the general-purpose functions and
;; commands added by Supremacs.  Some modules define additional module-specific
;; functions and commands.
;;
;; Note that many of the original core Supremacs commands were extracted to the
;; crux package (Supremacs installs it automatically).  Supremacs's auto-save
;; functionality was extracted to the super-save package.

;;; License:

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Code:

(require 'cl-lib)

(defun supremacs-buffer-mode (buffer-or-name)
  "Retrieve the `major-mode' of BUFFER-OR-NAME."
  (with-current-buffer buffer-or-name
    major-mode))

(defun supremacs-search (query-url prompt)
  "Open the search url constructed with the QUERY-URL.
PROMPT sets the `read-string prompt."
  (browse-url
   (concat query-url
           (url-hexify-string
            (if mark-active
                (buffer-substring (region-beginning) (region-end))
              (read-string prompt))))))

(defmacro supremacs-install-search-engine (search-engine-name search-engine-url search-engine-prompt)
  "Given some information regarding a search engine, install the interactive command to search through them"
  `(defun ,(intern (format "supremacs-%s" search-engine-name)) ()
       ,(format "Search %s with a query or region if any." search-engine-name)
       (interactive)
       (supremacs-search ,search-engine-url ,search-engine-prompt)))

(supremacs-install-search-engine "google"     "http://www.google.com/search?q="              "Google: ")
(supremacs-install-search-engine "youtube"    "http://www.youtube.com/results?search_query=" "Search YouTube: ")
(supremacs-install-search-engine "github"     "https://github.com/search?q="                 "Search GitHub: ")
(supremacs-install-search-engine "duckduckgo" "https://duckduckgo.com/?t=lm&q="              "Search DuckDuckGo: ")

(defun supremacs-recompile-init ()
  "Byte-compile all your dotfiles again."
  (interactive)
  (byte-recompile-directory supremacs-dir 0))

(defvar supremacs-tips
  '("Press <C-c o> to open a file with external program."
    "Press <C-c p f> to navigate a project's files."
    "Press <s-r> to open a recently visited file."
    "Press <C-c p s g> to run grep on a project."
    "Press <C-c p p> to switch between projects."
    "Press <C-=> to expand the selected region."
    "Press <C-c C-/ g> to search in Google."
    "Press <C-c C-/ h> to search in GitHub."
    "Press <C-c C-/ y> to search in YouTube."
    "Press <C-c C-/ d> to search in DuckDuckGo."
    "Press <C-c r> to rename the current buffer and the file it's visiting if any."
    "Press <C-c t> to open a terminal in Emacs."
    "Press <C-c k> to kill all the buffers, but the active one."
    "Press <C-x g> to run magit-status."
    "Press <C-c D> to delete the current file and buffer."
    "Press <C-c s> to swap two windows."
    "Press <S-RET> or <M-o> to open a line beneath the current one."
    "Press <s-o> to open a line above the current one."
    "Press <C-c C-z> in a Elisp buffer to launch an interactive Elisp shell."
    "Press <C-Backspace> to kill a line backwards."
    "Press <C-S-Backspace> or <s-k> to kill the whole line."
    "Press <s-j> or <C-^> to join lines."
    "Press <s-.> or <C-c v> to jump to the start of a word in any visible window."
    "Press <f12> to toggle the menu bar."
    "Explore the Supremacs menu to find out about some of Supremacs extensions to Emacs."
    "Access the official Emacs manual by pressing <C-h r>."))

(defun supremacs-tip-of-the-day ()
  "Display a random entry from `supremacs-tips'."
  (interactive)
  (when (and supremacs-tips (not (window-minibuffer-p)))
    ;; pick a new random seed
    (random t)
    (message
     (concat "Supremacs tip: " (nth (random (length supremacs-tips)) supremacs-tips)))))

(defun supremacs-eval-after-init (form)
  "Add `(lambda () FORM)' to `after-init-hook'.

    If Emacs has already finished initialization, also eval FORM immediately."
  (let ((func (list 'lambda nil form)))
    (add-hook 'after-init-hook func)
    (when after-init-time
      (eval form))))

(require 'epl)

(defun supremacs-update ()
  "Update Supremacs to its latest version."
  (interactive)
  (when (y-or-n-p "Do you want to update Supremacs? ")
    (message "Updating installed packages...")
    (epl-upgrade)
    (message "Updating Supremacs...")
    (cd supremacs-dir)
    (shell-command "git pull")
    (supremacs-recompile-init)
    (message "Update finished. Restart Emacs to complete the process.")))

(defun supremacs-update-packages (&optional arg)
  "Update Supremacs's packages.
This includes package installed via `supremacs-require-package'.

With a prefix ARG updates all installed packages."
  (interactive "P")
  (when (y-or-n-p "Do you want to update Supremacs's packages? ")
    (if arg
        (epl-upgrade)
      (epl-upgrade (cl-remove-if-not (lambda (p) (memq (epl-package-name p) supremacs-packages))
                                     (epl-installed-packages))))
    (message "Update finished. Restart Emacs to complete the process.")))

(defun supremacs-wrap-with (s)
  "Create a wrapper function for smartparens using S."
  `(lambda (&optional arg)
     (interactive "P")
     (sp-wrap-with-pair ,s)))

(provide 'supremacs-core)
;;; supremacs-core.el ends here
