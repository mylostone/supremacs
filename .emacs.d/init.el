;;; init.el --- Supremacs's configuration entry point.
;;
;; Copyright (c) 2011-2022 Bozhidar Batsov
;;
;; Author: Bozhidar Batsov <bozhidar@batsov.com>
;; URL: https://github.com/bbatsov/supremacs
;; Version: 1.1.0
;; Keywords: convenience

;; This file is not part of GNU Emacs.

;;; Commentary:

;; This file simply sets up the default load path and requires
;; the various modules defined within Emacs Supremacs.

;;; License:

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Code:

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
                                        ;(package-initialize)

(defvar supremacs-user
  (getenv
   (if (equal system-type 'windows-nt) "USERNAME" "USER")))

(message "[Supremacs] Supremacs is powering up... Be patient, Master %s!" supremacs-user)

(when (version< emacs-version "25.1")
  (error "[Supremacs] Supremacs requires GNU Emacs 25.1 or newer, but you're running %s" emacs-version))

;; Always load newest byte code
(setq load-prefer-newer t)

;; Define Supremacs's directory structure
(defvar supremacs-dir (file-name-directory load-file-name)
  "The root dir of the Emacs Supremacs distribution.")
(defvar supremacs-core-dir (expand-file-name "core" supremacs-dir)
  "The home of Supremacs's core functionality.")
(defvar supremacs-modules-dir (expand-file-name  "modules" supremacs-dir)
  "This directory houses all of the built-in Supremacs modules.")
(defvar supremacs-personal-dir (expand-file-name "personal" supremacs-dir)
  "This directory is for your personal configuration.

Users of Emacs Supremacs are encouraged to keep their personal configuration
changes in this directory.  All Emacs Lisp files there are loaded automatically
by Supremacs.")
(defvar supremacs-personal-preload-dir (expand-file-name "preload" supremacs-personal-dir)
  "This directory is for your personal configuration, that you want loaded before Supremacs.")
(defvar supremacs-vendor-dir (expand-file-name "vendor" supremacs-dir)
  "This directory houses packages that are not yet available in ELPA (or MELPA).")
(defvar supremacs-savefile-dir (expand-file-name "savefile" user-emacs-directory)
  "This folder stores all the automatically generated save/history-files.")
(defvar supremacs-modules-file (expand-file-name "supremacs-modules.el" supremacs-personal-dir)
  "This file contains a list of modules that will be loaded by Supremacs.")

(unless (file-exists-p supremacs-savefile-dir)
  (make-directory supremacs-savefile-dir))

(defun supremacs-add-subfolders-to-load-path (parent-dir)
  "Add all level PARENT-DIR subdirs to the `load-path'."
  (dolist (f (directory-files parent-dir))
    (let ((name (expand-file-name f parent-dir)))
      (when (and (file-directory-p name)
                 (not (string-prefix-p "." f)))
        (add-to-list 'load-path name)
        (supremacs-add-subfolders-to-load-path name)))))

;; add Supremacs's directories to Emacs's `load-path'
(add-to-list 'load-path supremacs-core-dir)
(add-to-list 'load-path supremacs-modules-dir)
(add-to-list 'load-path supremacs-vendor-dir)
(supremacs-add-subfolders-to-load-path supremacs-vendor-dir)

;; reduce the frequency of garbage collection by making it happen on
;; each 50MB of allocated data (the default is on every 0.76MB)
(setq gc-cons-threshold 50000000)

;; warn when opening files bigger than 100MB
(setq large-file-warning-threshold 100000000)

;; preload the personal settings from `supremacs-personal-preload-dir'
(when (file-exists-p supremacs-personal-preload-dir)
  (message "[Supremacs] Loading personal configuration files in %s..." supremacs-personal-preload-dir)
  (mapc 'load (directory-files supremacs-personal-preload-dir 't "^[^#\.].*el$")))

(message "[Supremacs] Loading Supremacs's core modules...")

;; load the core stuff
(require 'supremacs-packages)
(require 'supremacs-custom)  ;; Needs to be loaded before core, editor and ui
(require 'supremacs-ui)
(require 'supremacs-core)
(require 'supremacs-mode)
(require 'supremacs-editor)
(require 'supremacs-global-keybindings)

;; macOS specific settings
(when (eq system-type 'darwin)
  (require 'supremacs-macos))

;; Linux specific settings
(when (eq system-type 'gnu/linux)
  (require 'supremacs-linux))

;; WSL specific setting
(when (and (eq system-type 'gnu/linux) (getenv "WSLENV"))
  (require 'supremacs-wsl))

;; Windows specific settings
(when (eq system-type 'windows-nt)
  (require 'supremacs-windows))

(message "[Supremacs] Loading Supremacs's additional modules...")

;; the modules
(if (file-exists-p supremacs-modules-file)
    (load supremacs-modules-file)
  (message "[Supremacs] Missing personal modules file %s" supremacs-modules-file)
  (message "[Supremacs] Falling back to the bundled example file sample/supremacs-modules.el")
  (message "[Supremacs] You should copy this file to your personal configuration folder and tweak it to your liking")
  (load (expand-file-name "sample/supremacs-modules.el" supremacs-dir)))

;; config changes made through the customize UI will be stored here
(setq custom-file (expand-file-name "custom.el" supremacs-personal-dir))

;; load the personal settings (this includes `custom-file')
(when (file-exists-p supremacs-personal-dir)
  (message "[Supremacs] Loading personal configuration files in %s..." supremacs-personal-dir)
  (mapc 'load (delete
               supremacs-modules-file
               (directory-files supremacs-personal-dir 't "^[^#\.].*\\.el$"))))

(message "[Supremacs] Supremacs is ready to do thy bidding, Master %s!" supremacs-user)

;; Patch security vulnerability in Emacs versions older than 25.3
(when (version< emacs-version "25.3")
  (with-eval-after-load "enriched"
    (defun enriched-decode-display-prop (start end &optional param)
      (list start end))))

(supremacs-eval-after-init
 ;; greet the use with some useful tip
 (run-at-time 5 nil 'supremacs-tip-of-the-day))

;;; init.el ends here
